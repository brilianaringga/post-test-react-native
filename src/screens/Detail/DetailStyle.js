import {StyleSheet} from 'react-native';

export const Styles = StyleSheet.create({
  container: {
    padding: 15,
  },
  articleImage: {
    width: '100%',
    height: 250,
    borderRadius: 15,
    marginBottom: 15,
  },
  source: {
    marginBottom: 10,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 10,
  },
});
