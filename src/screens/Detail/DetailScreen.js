import React, {useEffect} from 'react';
import {Image, Text, View} from 'react-native';
import {Styles} from './DetailStyle';

const DetailScreen = (props) => {
  const {article} = props.route.params;

  useEffect(() => {
    props.navigation.setOptions({title: 'Detail Page'});
  });

  return (
    <View style={Styles.container}>
      <Image source={{uri: article.urlToImage}} style={Styles.articleImage} />
      <Text style={Styles.source}>{article.source.name}</Text>
      <Text style={Styles.title}>{article.title}</Text>
      <Text style={Styles.con}>{article.content}</Text>
    </View>
  );
};

export default DetailScreen;
