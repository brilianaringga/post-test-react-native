import {StyleSheet} from 'react-native';

export const Styles = StyleSheet.create({
  headerTitleStyle: {
    fontWeight: 'bold',
    fontSize: 20,
  },
  headerStyle: {
    // height: 120,
  },
  headerAvatar: {
    backgroundColor: 'gray',
    width: 40,
    height: 40,
    borderRadius: 20,
    marginLeft: 15,
  },
  container: {
    padding: 15,
  },
  articleImage: {
    width: '100%',
    height: 200,
    marginBottom: 5,
    borderRadius: 15,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 5,
  },
  descripion: {
    fontSize: 16,
  },
});
