import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {Image, Text, View} from 'react-native';
import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler';
import {Styles} from './HomeStyle';

const HomeScreen = (props) => {
  const username = 'Brilian Aringga';
  let [articles, setArticles] = useState({articles: []});

  useEffect(() => {
    async function fetchData() {
      const response = await axios.get(
        'http://newsapi.org/v2/everything?q=react%20native&apiKey=3eab490bd03340e682b2ac40db33e321',
      );

      const {data} = response;
      setArticles(data);
    }

    fetchData();
  }, []);

  useEffect(() => {
    props.navigation.setOptions({
      title: `Hello, ${username}`,
      headerTitleAlign: 'left',
      headerTitleStyle: {...Styles.headerTitleStyle},
      //   headerStyle: {...Styles.headerStyle},
      headerLeft: () => (
        <View>
          <View style={Styles.headerAvatar} />
        </View>
      ),
    });
  }, [props, username]);

  const openDetail = (article) => {
    props.navigation.navigate('Detail', {article: article});
  };

  return (
    <View>
      <ScrollView>
        {articles.articles.map((article, index) => {
          return (
            <TouchableOpacity
              key={index}
              style={Styles.container}
              onPress={() => openDetail(article)}>
              <Image
                source={{uri: article.urlToImage}}
                style={Styles.articleImage}
              />
              <Text style={Styles.title}>{article.title}</Text>
              <Text style={Styles.description}>{article.description}</Text>
            </TouchableOpacity>
          );
        })}
      </ScrollView>
    </View>
  );
};

export default HomeScreen;
